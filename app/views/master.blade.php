<!DOCTYPE html>
<html>
    <head>
        <title>@yield('title', 'Laravel')</title>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0-alpha1/jquery.min.js"></script>

        <script>
            jQuery(document).ready( function() {

                jQuery('.btn-danger').click( function() {
                    if (! confirm('Adakah anda pasti ingin memadam rekod ini?')) {
                        return false;
                    }
                });

            });
        </script>
    </head>
    <body>
        <div class="container">
            @yield('content')
        </div>
    </body>
</html>