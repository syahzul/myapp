@extends('master')

@section('title', 'Create User')

@section('content')

    {{ Form::open(['url' => 'user', 'class' => 'form-horizontal']) }}

        <p>
            {{ Form::label('name', 'Name') }}
            {{ Form::text('name', null, ['class' => 'form-control', 'id' => 'name']) }}

            @if ($errors->has('name'))
                <span class="text-danger">
                    {{ $errors->first('name') }}
                </span>
            @endif
        </p>

        <p>
            {{ Form::label('email', 'Email') }}
            {{ Form::email('email', null, ['class' => 'form-control']) }}

            @if ($errors->has('email'))
                <span class="text-danger">
                    {{ $errors->first('email') }}
                </span>
            @endif
        </p>

        <p>
            {{ Form::label('password', 'Password') }}
            {{ Form::password('password', ['class' => 'form-control']) }}

            @if ($errors->has('password'))
                <span class="text-danger">
                    {{ $errors->first('password') }}
                </span>
            @endif
        </p>

        <p>
            {{ Form::label('password_confirmation', 'Repeat Password') }}
            {{ Form::password('password_confirmation', ['class' => 'form-control']) }}
        </p>

        <p>
            {{ Form::submit('Save User', ['class' => 'btn btn-primary']) }}
        </p>

    {{ Form::close() }}

@stop