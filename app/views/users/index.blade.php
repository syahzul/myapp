@extends('master')

@section('title', 'User List')

@section('content')
    <h1>User List</h1>
    <table class="table">
        <thead>
            <tr>
                <th>Name</th>
                <th>Email</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            @foreach ($users as $user)
            <tr>
                <td>{{ $user->name }}</td>
                <td>{{ $user->email }}</td>
                <td>
                    <a href="{{ URL::route('user.edit', $user->id) }}" class="btn btn-xs btn-warning">
                        Edit
                    </a>

                    <a href="{{ URL::route('user.delete', $user->id) }}" class="btn btn-xs btn-danger">
                        Delete
                    </a>

                </td>
            </tr>
            @endforeach
        </tbody>
        <tfoot>
            <tr>
                <td colspan="3">
                    {{ $users->links() }}
                </td>
            </tr>
        </tfoot>
    </table>
@stop