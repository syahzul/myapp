<?php

class UserController extends BaseController
{
	public function index()
	{
		$users = User::orderBy('name', 'asc')->paginate(2);

		return View::make('users.index', compact('users'));
	}

	public function create()
	{
		return View::make('users.create');
	}

	public function store()
	{
		// Validation rules
		$rules = [
			'email'     => 'required|unique:users,email|email',
			'name'      => 'required|max:150',
			'password'  => 'required|min:8|confirmed'
		];

		// Get all input from form
		$input = Input::all();

		// Custom validation messages
		$messages = [
			'email.unique'   => 'Emel telah digunakan. Sila pilih email yang lain.',
			'email.required' => 'Sila isikan alamat emel yang sah.',
			'email.email'    => 'Sila emel menepati format.',

			'name.required'  => 'Sila isikan nama penuh anda.',
			'name.max'       => 'Maksimum karakter adalah 150 sahaja.',

			'password.required'  => 'Sila isikan kata laluan anda.',
			'password.min'  => 'Kata laluan mestilah sekurang-kurangnya 8 aksara.',
		];

		$validation = Validator::make($input, $rules, $messages);

		if ($validation->fails()) {
			return Redirect::back()->withInput()->withErrors($validation);
		}

		// Encrypt user password before storing to DB
		$input['password'] = Hash::make($input['password']);

		User::create($input);

		return Redirect::to('/user');
	}

	public function edit($id)
	{
		$user = User::find($id);

		return View::make('users.edit', compact('user'));
	}

	public function update($id)
	{
		// Validation rules
		$rules = [
			'email'     => 'required|unique:users,email,'.$id.'|email',
			'name'      => 'required|max:150',
			'password'  => 'min:8|confirmed'
		];

		// Get all input from form
		$input = Input::all();

		// Custom validation messages
		$messages = [
			'email.unique'   => 'Emel telah digunakan. Sila pilih email yang lain.',
			'email.required' => 'Sila isikan alamat emel yang sah.',
			'email.email'    => 'Sila emel menepati format.',

			'name.required'  => 'Sila isikan nama penuh anda.',
			'name.max'       => 'Maksimum karakter adalah 150 sahaja.',

			'password.min'  => 'Kata laluan mestilah sekurang-kurangnya 8 aksara.',
		];

		$validation = Validator::make($input, $rules, $messages);

		if ($validation->fails()) {
			return Redirect::back()->withInput()->withErrors($validation);
		}

		// Kalau password ada isi
		if (Input::has('password')) {
			// update password
			// Encrypt user password before storing to DB
			$input['password'] = Hash::make($input['password']);
		}
		// Kalau tidak
		else {
			// tak perlu buat apa apa
			unset($input['password']);
		}

		$user = User::find($id);

		$user->update($input);

		return Redirect::to('/user');
	}

	public function delete($id)
	{
		$user = User::find($id);

		$user->delete();

		return Redirect::to('/user');
	}

}