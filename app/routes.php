<?php

Route::get('/', 'HomeController@showWelcome');

Route::resource('user', 'UserController');

Route::get('user/{user}/delete', [
	'uses' => 'UserController@delete',
	'as'   => 'user.delete'
]);
